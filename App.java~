package org.inguelberth.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.ToolBar;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.collections.FXCollections;

import javafx.event.EventHandler;
import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;

import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.manejadores.ManejadorContacto;
import org.inguelberth.db.Conexion;
import org.inguelberth.beans.Contacto;

public class App extends Application implements EventHandler<Event>{

	private Scene primaryScene;
	
	private TabPane tpPrincipal;
	private Tab tbLogin, tbContactos, tbCRUD;

	private TextField tfNombre;
	private TextField tfNombreA, tfApellidoA, tfTelefonoA;
	private Label lblNombreA, lblApellidoA, lblTelefonoA;
	private PasswordField pfContra;
	private Label lblNombre, lblContra;
	private Conexion cnx;
	private TableView<Contacto> tvContactos;
	private ToolBar tlbContactos;
	private BorderPane bpPrincipal;
	private Button btnA, btnD, btnU, btnL;
	//false=AGREGAR, true=MODIFICAR
	private boolean estadoCRUD;

	private GridPane gpLogin, gpCRUD;

	private ManejadorUsuario mUsuario;
	private ManejadorContacto mContacto;

	public void start(Stage primaryStage){
		cnx = new Conexion();
		
		mUsuario = new ManejadorUsuario(cnx);
		mContacto = new ManejadorContacto(cnx);

		bpPrincipal = new BorderPane();
		
		tpPrincipal = new TabPane();

		tbLogin = new Tab("Login");

		gpLogin = new GridPane();

		tfNombre = new TextField();
		tfNombre.setPromptText("nombre de usuario");
		tfNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);

		pfContra = new PasswordField();
		pfContra.setPromptText("clave de usuario");
		pfContra.addEventHandler(KeyEvent.KEY_RELEASED, this);

		lblNombre = new Label("Nombre: ");
		lblContra = new Label("Clave: ");

		gpLogin.add(lblNombre, 0,0);
		gpLogin.add(tfNombre, 1, 0);
		gpLogin.add(lblContra, 0,1);
		gpLogin.add(pfContra, 1, 1);

		tbLogin.setContent(gpLogin);
		
		tvContactos = new TableView<Contacto>(mContacto.getListaDeContactos());
		tvContactos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		TableColumn<Contacto, String> columnaNombre=new TableColumn<Contacto, String>("NOMBRE");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Contacto, String>("nombre"));
		TableColumn<Contacto, String> columnaApellido=new TableColumn<Contacto, String>("APELLIDO");
		columnaApellido.setCellValueFactory(new PropertyValueFactory<Contacto, String>("apellido"));
		TableColumn<Contacto, String> columnaTelefono=new TableColumn<Contacto, String>("TELEFONO/CELULAR");
		columnaTelefono.setCellValueFactory(new PropertyValueFactory<Contacto, String>("telefono"));

		tvContactos.getColumns().setAll(columnaNombre, columnaApellido, columnaTelefono);

		tbContactos = new Tab("Contactos");
		tbContactos.setContent(tvContactos);

		gpCRUD = new GridPane();
		tfNombreA=new TextField();
		tfNombreA.addEventHandler(KeyEvent.KEY_RELEASED, this);
		tfApellidoA=new TextField();
		tfApellidoA.addEventHandler(KeyEvent.KEY_RELEASED, this);
		tfTelefonoA=new TextField();
		tfTelefonoA.addEventHandler(KeyEvent.KEY_RELEASED, this);
		
		lblNombreA = new Label("Nombre: ");
		lblApellidoA = new Label("Apellido: ");
		lblTelefonoA = new Label("Telefono: ");
	
		gpCRUD.add(lblNombreA, 0,0);
		gpCRUD.add(tfNombreA, 1,0);
		
		gpCRUD.add(lblApellidoA, 0,1);
		gpCRUD.add(tfApellidoA, 1,1);

		gpCRUD.add(lblTelefonoA, 0,2);
		gpCRUD.add(tfTelefonoA, 1,2);
	
		tbCRUD = new Tab("Agregar");
		tbCRUD.setContent(gpCRUD);

		tlbContactos = new ToolBar();
		btnA = new Button("Agregar");
		btnA.addEventHandler(ActionEvent.ACTION, this);
		btnD = new Button("Eliminar");
		btnD.addEventHandler(ActionEvent.ACTION, this);
		btnU = new Button("Actualizar");
		btnU.addEventHandler(ActionEvent.ACTION, this);
		btnL = new Button("Desconectar");
		btnL.addEventHandler(ActionEvent.ACTION, this);	
		tlbContactos.getItems().addAll(btnA, btnD, btnU, btnL);

		tpPrincipal.getTabs().add(tbLogin);

		//bpPrincipal.setTop(tbCRUD);
		bpPrincipal.setCenter(tpPrincipal);

		primaryScene = new Scene(bpPrincipal);
		
		primaryStage.setScene(primaryScene);

		primaryStage.show();
	}
	private boolean verificarDatos(){
		return !tfNombre.getText().trim().equals("") & !pfContra.getText().trim().equals("");
	}
	public boolean verificarTextdCRUD(){
		return !tfNombreA.getText().trim().equals("") & !tfApellidoA.getText().trim().equals("") & !tfTelefonoA.getText().trim().equals("");
	}
	public void setTextCRUD(Contacto contacto){
		tfNombreA.setText(contacto.getNombre());
		tfApellidoA.setText(contacto.getApellido());
		tfTelefonoA.setText(contacto.getTelefono());
	}
	public void handle(Event event){
		if(event instanceof KeyEvent){
			if(((KeyEvent)event).getCode() == KeyCode.ENTER){
				if(event.getSource().equals(tfNombre) || event.getSource().equals(pfContra)){
					if(verificarDatos()){
						Stage dialogo = new Stage();
						if(mUsuario.autenticar(tfNombre.getText(), pfContra.getText())){
							dialogo.setScene(new Scene(new Label("Bienvenido "+tfNombre.getText())));
							mContacto.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
							tpPrincipal.getTabs().add(tbContactos);
							tpPrincipal.getTabs().remove(tbLogin);
							bpPrincipal.setTop(tlbContactos);
						}else{
							dialogo.setScene(new Scene(new Label("Verifique sus credenciales :(")));
						}
						dialogo.show();
					}
				}else if(event.getSource().equals(tfNombreA) || event.getSource().equals(tfApellidoA) || event.getSource().equals(tfTelefonoA)){
					if(verificarTextdCRUD()){
						Contacto contactoAgregar = new Contacto(mUsuario.getUsuarioAutenticado().getIdUsuario(), 0, tfNombreA.getText(), tfApellidoA.getText(), tfTelefonoA.getText());
						if(estadoCRUD){
							contactoAgregar.setIdContacto(tvContactos.getSelectionModel().getSelectedItems().get(0).getIdContacto());
							mContacto.modificarContacto(contactoAgregar);
						}else{
							mContacto.agregarContacto(contactoAgregar);
						}
						tpPrincipal.getTabs().remove(tbCRUD);
						setTextCRUD(new Contacto());
					}
				}
			}
		}else if(event instanceof ActionEvent){
			if(event.getSource().equals(btnL)){
				mUsuario.desautenticar();
				mContacto.setUsuarioConectado(null);
				tpPrincipal.getTabs().add(tbLogin);
				tpPrincipal.getTabs().remove(tbContactos);
			}else if(event.getSource().equals(btnD)){
				ObservableList<Contacto> contactoSeleccionados = tvContactos.getSelectionModel().getSelectedItems();
				ArrayList<Contacto> listaNoObserbable = new ArrayList<Contacto>();			
				FXCollections.copy(contactoSeleccionados, listaNoObserbable);
				for(Contacto contacto : listaNoObserbable){
					mContacto.eliminarContacto(contacto);
				}
				
			}else if(event.getSource().equals(btnA)){
				tpPrincipal.getTabs().add(tbCRUD);
				tpPrincipal.getSelectionModel().select(tbCRUD);
				estadoCRUD = false;
			}else if(event.getSource().equals(btnU)){

				ObservableList<Contacto> contactoSeleccionados = tvContactos.getSelectionModel().getSelectedItems();
				if(contactoSeleccionados.size()==1){
					estadoCRUD = true;
					setTextCRUD(contactoSeleccionados.get(0));
					tpPrincipal.getTabs().add(tbCRUD);
					tpPrincipal.getSelectionModel().select(tbCRUD);
				}
			}
		}
	}
	public void setMUsuario(ManejadorUsuario mUsuario){
		this.mUsuario = mUsuario;
	}
}
