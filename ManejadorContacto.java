package org.inguelberth.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.inguelberth.beans.Contacto;
import org.inguelberth.beans.Usuario;
import org.inguelberth.db.Conexion;

public class ManejadorContacto{
	private ObservableList<Contacto> listaDeContacto;
	private Conexion cnx;
	private Usuario usuarioConectado;
	public ManejadorContacto(Conexion cnx){
		this.listaDeContacto = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarListaDeContactos();
	}
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	public void actualizarListaDeContactos(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT idContacto, idUsuario, nombre, apellido, telefotno FROM Contactos WHERE idUsuario="+usuarioConectado.getIdUsuario());
			if(resultado!=null){
				listaDeContacto.clear();
				try{
					while(resultado.next()){
						System.out.println("RESULTADO RESULTSET "+resultado.getString("telefotno"));
						Contacto contacto=new Contacto(resultado.getInt("idUsuario"), resultado.getInt("idContacto"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getString("telefotno"));
						listaDeContacto.add(contacto);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}else{
			listaDeContacto.clear();
		}		
	}
	public ObservableList<Contacto> getListaDeContactos(){
		return listaDeContacto;
	}
	public void eliminarContacto(Contacto contacto){
		cnx.ejecutarSentencia("DELETE FROM Contactos WHERE idContacto="+contacto.getIdContacto());
		actualizarListaDeContactos();
	}
	public void agregarContacto(Contacto contacto){
		cnx.ejecutarSentencia("INSERT INTO Contactos(idUsuario,nombre, apellido, telefotno) VALUES ("+contacto.getIdUsuario()+",'"+contacto.getNombre()+"', '"+contacto.getApellido()+"', '"+contacto.getTelefono()+"')");
		actualizarListaDeContactos();
	}
	public void modificarContacto(Contacto contacto){
		cnx.ejecutarSentencia("UPDATE Contactos SET nombre='"+contacto.getNombre()+"', apellido='"+contacto.getApellido()+"', telefotno='"+contacto.getTelefono()+"' WHERE idContacto="+contacto.getIdContacto());
		actualizarListaDeContactos();
	}
	
}
