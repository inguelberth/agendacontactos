package org.inguelberth.db;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion{
	private static Conexion instancia;
	private Connection cnx;
	private Statement stn;
	public Conexion(){
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			cnx = DriverManager.getConnection("jdbc:sqlserver://192.168.107.120\\KINAL;databasename=dbContactos", "guate", "2014");
			stn = cnx.createStatement();
			
		}catch(SQLException sql){
			sql.printStackTrace();
		}catch(ClassNotFoundException cl){
			System.out.println("Driver no encontrado");
		}
	}
	public void ejecutarSentencia(String sentencia){
		try{
			stn.execute(sentencia);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
	}
	public ResultSet ejecutarConsulta(String consulta){
		ResultSet resultado = null;
		try{
			resultado = stn.executeQuery(consulta);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
		return resultado;
	}
	
}
